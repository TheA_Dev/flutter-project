import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ginseng_app/class_items.dart';
import 'package:google_fonts/google_fonts.dart';

class MyOrderPage extends StatefulWidget {
  @override
  _MyOrderPageState createState() => _MyOrderPageState();
}

final foodpandastyle = GoogleFonts.lato(
  color: Colors.black,
  fontSize: 23,
  fontWeight: FontWeight.bold,
);
final EgetStyle = GoogleFonts.lato(
  color: Colors.black45,
  fontSize: 14,
  letterSpacing: 0.50,
  fontWeight: FontWeight.bold,
);
final DateStyle = GoogleFonts.lato(
    color: Colors.grey.shade700,
    fontSize: 12,
    wordSpacing: 0.50,
    fontWeight: FontWeight.bold);
final priceStyle = GoogleFonts.lato(
  color: Colors.black,
  fontSize: 17,
  letterSpacing: 0.60,
  fontWeight: FontWeight.bold,
);

class _MyOrderPageState extends State<MyOrderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        toolbarHeight: 70,
        elevation: 0,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          iconSize: 25,
          color: Colors.black,
          icon: Icon(Icons.arrow_back_ios),
        ),
        title: Text(
          "Order History",
          style: GoogleFonts.lato(
            color: Colors.black,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: [
          InkWell(
            onTap: () {},
            splashColor: Colors.blueGrey,
            borderRadius: BorderRadius.circular(8),
            child: Center(
              child: Text(
                "Clear",
                style: GoogleFonts.lato(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          )
        ],
      ),
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: [
          Egets_History(context),
          __foodPanda_history(context),
          My_Creation(context),
        ],
      ),
    );
  }


  SliverToBoxAdapter My_Creation(BuildContext context) {
    return SliverToBoxAdapter(
        child: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: ListView.builder(
          itemCount: More_By.length,
          physics: BouncingScrollPhysics(),
          itemBuilder: (context, snapshot) {
            return Container(
              height: 250,
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 10, bottom: 5),
              decoration: BoxDecoration(
                //color: Colors.indigo,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Stack(
                children: [
                  //Image name date
                  Container(
                    height: 150,
                    // width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                        image: DecorationImage(
                            image: AssetImage("assets/moreBy/fd1.jpg"),
                            fit: BoxFit.cover)),
                  ),
                  //Blur Image
                  ClipRect(
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaY: 2, sigmaX: 2),
                      child: Container(
                        alignment: Alignment.center,
                        height: 150,
                        decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.50)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Italian Food",
                              style: GoogleFonts.lato(
                                  color: Colors.white,
                                  fontSize: 20,
                                  letterSpacing: 0.70,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "Today, 4:20 PM - 4:50 PM",
                              style: GoogleFonts.lato(
                                  color: Colors.grey,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  //shop
                  Positioned(
                      bottom: 32,
                      child: Row(
                        children: [
                          CircleAvatar(
                            backgroundColor: Colors.white,
                            radius: 30,
                            child: Image(
                                image: AssetImage("assets/more/fd-logo.png")),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Italian Delicious Food",
                            style: priceStyle,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          InkWell(
                            onTap: () {},
                            splashColor: Colors.red.shade300,
                            radius: 10,
                            child: Text("OPEN",
                                style: GoogleFonts.lato(
                                  color: Colors.red,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline,
                                  letterSpacing: 0.50,
                                )),
                          ),
                        ],
                      )),
                  //re-order
                  Positioned(
                      bottom: 3,
                      left: 5,
                      child: InkWell(
                        onTap: () {},
                        child: Container(
                            height: 22,
                            width: 80,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(5)),
                            child: Text(
                              "RE-ORDER",
                              style: GoogleFonts.lato(
                                  color: Colors.white,
                                  fontSize: 13,
                                  letterSpacing: 0.50,
                                  fontWeight: FontWeight.bold),
                            )),
                      )),
                  //price
                  Positioned(
                      bottom: 38,
                      right: 5,
                      child: Text(
                        "\$10.30",
                        style: priceStyle,
                      )),
                  //deleted day
                  Positioned(
                      bottom: 8,
                      right: 5,
                      child: GestureDetector(
                        onTap: () {
                          setState(() {});
                        },
                        child: Text(
                          "30 days",
                          style: GoogleFonts.lato(
                            color: Colors.black,
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )),
                ],
              ),
            );
          }),
    ));
  }


  SliverToBoxAdapter Egets_History(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 220,
        margin: EdgeInsets.all(8),
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          //color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: Colors.black12,
            width: 0.80,
          ),
        ),
        child: Stack(
          children: [
            //Shop and Date
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image(
                  height: 45,
                  image: AssetImage("assets/more/amazon.png"),
                ),
                SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Cafe' Amazon (National Library)", style: priceStyle),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "25 - 09 - 2021 09: 15: 25",
                      style: DateStyle,
                    ),
                  ],
                ),
              ],
            ),
            //next arrow
            Positioned(
                right: 0,
                child: IconButton(
                    onPressed: () {},
                    color: Colors.blueGrey,
                    iconSize: 18,
                    icon: Icon(Icons.arrow_forward_ios))),
            //completed
            Positioned(
                top: 50,
                child: Row(
                  children: [
                    Icon(Icons.check),
                    Text("Completed"),
                  ],
                )),
            //Name
            Positioned(
              bottom: 75,
              child: Text("Amazon cappuchinu  Frappe", style: priceStyle),
            ),
            //$
            Positioned(
                right: 0,
                bottom: 85,
                child: Text(
                  "\$3.50",
                  style: priceStyle,
                )),
            //KHR
            Positioned(
                right: 0,
                bottom: 60,
                child: Text(
                  "\$3.50+KHR 2100",
                  style: priceStyle,
                )),
            // Contact shop , Re-Order , Rating
            Positioned(
                bottom: 6,
                right: 0,
                child: Row(
                  children: [
                    //contact shop
                    InkWell(
                      onTap: () {},
                      child: Container(
                          height: 35,
                          width: 130,
                          margin: EdgeInsets.only(right: 13),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.grey.shade400,
                          ),
                          child: Text(
                            "CONTACT SHOP",
                            style: EgetStyle,
                          )),
                    ),
                    //Re order
                    InkWell(
                      onTap: () {},
                      child: Container(
                          height: 35,
                          width: 85,
                          margin: EdgeInsets.only(right: 13),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.orange.shade500,
                          ),
                          child: Text(
                            "RE-ORDER",
                            style: GoogleFonts.lato(
                              color: Colors.white60,
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                            ),
                          )),
                    ),
                    //Ratings
                    InkWell(
                      onTap: () {},
                      child: Container(
                          height: 35,
                          width: 80,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.grey.shade400,
                          ),
                          child: Text(
                            "RATINGS",
                            style: EgetStyle,
                          )),
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  SliverToBoxAdapter __foodPanda_history(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        height: 130,
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.all(6),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                color: Colors.grey.shade500,
                blurRadius: 5,
                spreadRadius: 0,
                offset: Offset(1, 3)),
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Stack(
          children: [
            //shop , name ,  date
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Food Panda', style: foodpandastyle),
                Text(
                  'Koi Milk Tea',
                  style: GoogleFonts.lato(
                    color: Colors.black87,
                    fontSize: 18,
                  ),
                ),
                Text(
                  '23, September, 2021',
                  style: DateStyle,
                ),
              ],
            ),
            //Price
            Positioned(
              top: 10,
              right: 0,
              child: Text('\$9.0', style: foodpandastyle),
            ),
            //Reorder
            Positioned(
              bottom: 0,
              right: 0,
              child: Container(
                  height: 33,
                  width: 75,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.deepOrange),
                  child: Text(
                    'Reorder',
                    style: GoogleFonts.lato(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 0.50,
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
