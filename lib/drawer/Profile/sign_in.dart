import 'package:flutter/material.dart';
import 'package:ginseng_app/drawer/Profile/profile_Page.dart';
import 'package:ginseng_app/drawer/Profile/sign_up.dart';
import 'package:google_fonts/google_fonts.dart';

class Sign_In extends StatefulWidget {
  @override
  _Sign_InState createState() => _Sign_InState();
}

final singInStyle = GoogleFonts.lato(
  letterSpacing: 0.30,
  color: Colors.white,
  fontSize: 15,
  fontWeight: FontWeight.w500,
);
final HintStyle = GoogleFonts.lato(
  color: Colors.white60,
  fontSize: 15,
);
bool remeberMeValue = false;

class _Sign_InState extends State<Sign_In> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo.shade200,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //back pop
          Padding(
            padding: EdgeInsets.only(top: 55, left: 5),
            child: IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(Icons.arrow_back_ios),
              color: Colors.black,
              iconSize: 25,
              splashRadius: 10,
            ),
          ),
          //sign in Text
          Center(
            child: Text(
              "Sign In",
              style: GoogleFonts.lato(
                color: Colors.black,
                fontSize: 30,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.50,
              ),
            ),
          ),
          //Email
          Padding(
            padding: EdgeInsets.only(top: 20, left: 30, right: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Email",
                  style: singInStyle,
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(left: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.indigo.shade300),
                  child: TextField(
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      icon: Icon(
                        Icons.mail_outline_outlined,
                        color: Colors.blueGrey,
                        size: 25,
                      ),
                      hintText: "Email or Phone",
                      hintStyle: HintStyle,
                    ),
                  ),
                ),
              ],
            ),
          ),
          //Password
          Padding(
            padding: EdgeInsets.only(top: 20, left: 30, right: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Password",
                  style: singInStyle,
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(left: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.indigo.shade300),
                  child: TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      icon: Icon(
                        Icons.vpn_key_sharp,
                        color: Colors.blueGrey,
                        size: 25,
                      ),
                      hintText: "Password",
                      hintStyle: HintStyle,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(top: 5),
                    child: Text(
                      "Forget Password?",
                      style: singInStyle,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: [
                      Checkbox(
                        checkColor: Colors.white,
                        activeColor: Colors.indigo.shade500,
                        splashRadius: 15,
                        value: remeberMeValue,
                        onChanged: (value) {
                          setState(() {
                            remeberMeValue = value;
                          });
                        },
                      ),
                      Text(
                        "Remember Me",
                        style: singInStyle,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          //Login
          Padding(
            padding: EdgeInsets.only(top: 20, left: 30, right: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_)=>ProfilePage()));
                  },
                  child: Container(
                      height: 50,
                      padding: EdgeInsets.only(left: 5),
                      margin: EdgeInsets.only(left: 30, right: 30),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: Colors.white70,
                      ),
                      child: Text(
                        "Login",
                        style: GoogleFonts.lato(
                          color: Colors.indigo,
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                ),
                SizedBox(
                  height: 25,
                ),
                Text(
                  "OR",
                  style: GoogleFonts.lato(
                      color: Colors.black,
                      fontSize: 16.50,
                      letterSpacing: 0.50,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 25,
                ),
                Text(
                  "Sign In with",
                  style: GoogleFonts.lato(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 0.50),
                ),
                SizedBox(
                  height: 25,
                ),
                //FaceBook , Google
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {},
                      child: CircleAvatar(
                        radius: 25,
                        backgroundColor: Colors.white,
                        child: FittedBox(
                          fit: BoxFit.cover,
                          child: Image(
                            height: 25,
                            image: AssetImage("assets/icons/facebook.png"),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: CircleAvatar(
                        radius: 25,
                        backgroundColor: Colors.white,
                        child: FittedBox(
                          fit: BoxFit.cover,
                          child: Image(
                            height: 25,
                            image: AssetImage("assets/icons/google.png"),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Spacer(),
          Padding(
            padding: EdgeInsets.only(bottom: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Don\'t have an Account?",
                  style: GoogleFonts.lato(
                      color: Colors.white, fontSize: 18, letterSpacing: 0.70),
                ),
                SizedBox(
                  width: 7,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context, MaterialPageRoute(builder: (_) => Sign_Up()));
                  },
                  child: Text("Sign Up",
                      style: GoogleFonts.lato(
                        color: Colors.white,
                        fontSize: 20,
                        letterSpacing: 0.70,
                        fontWeight: FontWeight.bold,
                      )),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}