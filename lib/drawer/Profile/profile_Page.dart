import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

final nameStyle = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 18,
    fontWeight: FontWeight.bold,
    letterSpacing: 0.50);
final inforStyleA = GoogleFonts.lato(
  color: Colors.black,
  fontSize: 20,
  fontWeight: FontWeight.bold,
);
final inforStyleB = GoogleFonts.lato(
    color: Colors.blueGrey, fontSize: 17, fontWeight: FontWeight.w500);

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
          iconSize: 25,
          color: Colors.black,
          splashColor: Colors.indigo,
          splashRadius: 15,
        ),
        backgroundColor: Colors.white,
        toolbarHeight: 70,
        centerTitle: true,
        elevation: 0.10,
        title: Text(
          "Lee Min Hoo",
          style: nameStyle,
        ),
        actions: [
          Row(
            children: [
              InkWell(
                onTap: () {},
                borderRadius: BorderRadius.circular(20),
                radius: 10,
                splashColor: Colors.grey,
                child: Icon(
                  Icons.edit,
                  color: Colors.black,
                  size: 25,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              InkWell(
                onTap: () {},
                borderRadius: BorderRadius.circular(20),
                radius: 10,
                splashColor: Colors.grey,
                child: Icon(
                  Icons.favorite,
                  color: Colors.black,
                  size: 25,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              InkWell(
                onTap: () {},
                borderRadius: BorderRadius.circular(20),
                radius: 10,
                splashColor: Colors.grey,
                child: Image(
                    height: 25,
                    color: Colors.black,
                    image: AssetImage("assets/icons/save.png")),
              ),
              SizedBox(
                width: 10,
              ),
            ],
          )
        ],
      ),
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: [
          //Profile image & cover
          Container(
            height: 290,
            //color: Colors.green,
            child: Stack(
              children: [
                //Cover photo
                Container(
                  height: 200,
                  width: double.infinity,
                  margin: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(20)),
                    color: Colors.white70,
                    image: DecorationImage(
                      image: AssetImage("assets/slider/min.jpg"),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Stack(
                    children: [
                      //camera
                      Positioned(
                        bottom: 10,
                        right: 10,
                        child: GestureDetector(
                          onTap: () {},
                          child: Container(
                            height: 35,
                            width: 35,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                color: Colors.white,
                                border:
                                    Border.all(color: Colors.black, width: 2)),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Image(
                                height: 25,
                                color: Colors.black,
                                image: AssetImage("assets/icons/cam.png"),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                //profile photo
                Positioned.fill(
                    bottom: 0,
                    //left: MediaQuery.of(context).size.width/2-180,
                    //left: 120,
                   // width: MediaQuery.of(context).size.width,
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        height: 180,
                        width: 180,
                       // margin: EdgeInsets.only(left: MediaQuery.of(context).size.width/2,),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            border: Border.all(color: Colors.black, width: 3),
                            image: DecorationImage(
                              image: AssetImage("assets/slider/lee.png"),
                              fit: BoxFit.cover,
                            )),
                        child: Stack(
                          children: [
                            //camera
                            Positioned(
                              bottom: 10,
                              right: 10,
                              child: GestureDetector(
                                onTap: () {},
                                child: Container(
                                  height: 35,
                                  width: 35,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: Colors.white,
                                      border: Border.all(
                                          color: Colors.black, width: 2)),
                                  child: FittedBox(
                                    fit: BoxFit.scaleDown,
                                    child: Image(
                                      height: 25,
                                      color: Colors.black,
                                      image: AssetImage("assets/icons/cam.png"),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
          //name & bio
          Center(
              child: Padding(
            padding: EdgeInsets.only(top: 10, bottom: 5),
            child: Column(
              children: [
                Text(
                  "Lee Min Hoo",
                  style: nameStyle,
                ),
                Text(
                  "Bio",
                  style: GoogleFonts.lato(
                    color: Colors.grey,
                    fontSize: 17,
                  ),
                ),
              ],
            ),
          )),
          //Phone Number
          Container(
            height: 100,
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.only(left: 10, right: 10, bottom: 20),
            decoration: BoxDecoration(
              color: Colors.grey.shade200,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Mobile Number :",
                  style: inforStyleA,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "85593-559-030",
                  style: inforStyleB,
                ),
                Spacer(),
                Align(
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                      onTap: () {},
                      child: Text(
                        "Edit",
                        style: inforStyleB,
                      )),
                )
              ],
            ),
          ),
          //Email
          Container(
            height: 100,
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.only(left: 10, right: 10, bottom: 20),
            decoration: BoxDecoration(
              color: Colors.grey.shade200,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Email :",
                  style: inforStyleA,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "ginseng2021@gmail.com",
                  style: inforStyleB,
                ),
                Spacer(),
                Align(
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                      onTap: () {},
                      child: Text(
                        "Edit",
                        style: inforStyleB,
                      )),
                )
              ],
            ),
          ),
          //Address
          Container(
            height: 100,
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.only(left: 10, right: 10, bottom: 20),
            decoration: BoxDecoration(
              color: Colors.grey.shade200,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Address :",
                  style: inforStyleA,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Samdach Louis Em, Phnom Penh, street 282",
                  style: inforStyleB,
                ),
                Spacer(),
                Align(
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                      onTap: () {},
                      child: Text(
                        "Edit",
                        style: inforStyleB,
                      )),
                )
              ],
            ),
          ),
          ListTile(
            tileColor: Colors.blueGrey.shade200,
            leading: Image(
              height: 35,
              color: Colors.black,
              image: AssetImage("assets/icons/bonus.png"),
            ),
            title: Text("Bonus"),
          ),
          ListTile(
            tileColor: Colors.blueGrey.shade100,
            leading: Image(
              height: 40,
              color: Colors.black,
              image: AssetImage("assets/icons/wallet.png"),
            ),
            title: Text("Wallet"),
          ),
        ],
      ),
    );
  }
}
