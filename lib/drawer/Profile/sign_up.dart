import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
class Sign_Up extends StatefulWidget {
  @override
  _Sign_UpState createState() => _Sign_UpState();
}

final singInStyle = GoogleFonts.lato(
  letterSpacing: 0.30,
  color: Colors.white,
  fontSize: 15,
  fontWeight: FontWeight.w500,
);
final HintStyle = GoogleFonts.lato(
  color: Colors.white60,
  fontSize: 15,
);

class _Sign_UpState extends State<Sign_Up> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo.shade200,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //back pop
          Padding(
            padding: EdgeInsets.only(top: 55, left: 5),
            child: IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(Icons.arrow_back_ios),
              color: Colors.black,
              iconSize: 25,
              splashRadius: 10,
            ),
          ),
          //sign Up Text
          Center(
            child: Text(
              "Sign Up",
              style: GoogleFonts.lato(
                color: Colors.black,
                fontSize: 30,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.50,
              ),
            ),
          ),
          //Full Name
          Padding(
            padding: EdgeInsets.only(top: 20, left: 30, right: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Full Name",
                  style: singInStyle,
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(left: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.indigo.shade300),
                  child: TextField(
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      icon: Icon(
                        Icons.person,
                        color: Colors.blueGrey,
                        size: 25,
                      ),
                      hintText: "Enter your name",
                      hintStyle: HintStyle,
                    ),
                  ),
                ),
              ],
            ),
          ),
          //Phone Number
          Padding(
            padding: EdgeInsets.only(top: 20, left: 30, right: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Phone Number",
                  style: singInStyle,
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(left: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.indigo.shade300),
                  child: TextField(
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      icon: Icon(
                        Icons.phone_iphone_rounded,
                        color: Colors.blueGrey,
                        size: 25,
                      ),
                      hintText: "Enter your number",
                      hintStyle: HintStyle,
                    ),
                  ),
                ),
              ],
            ),
          ),
          //Email
          Padding(
            padding: EdgeInsets.only(top: 20, left: 30, right: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Email",
                  style: singInStyle,
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(left: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.indigo.shade300),
                  child: TextField(
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      icon: Icon(
                        Icons.mail_outline_outlined,
                        color: Colors.blueGrey,
                        size: 25,
                      ),
                      hintText: "Enter your email",
                      hintStyle: HintStyle,
                    ),
                  ),
                ),
              ],
            ),
          ),
          //Password
          Padding(
            padding: EdgeInsets.only(top: 20, left: 30, right: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Password",
                  style: singInStyle,
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(left: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.indigo.shade300),
                  child: TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      icon: Icon(
                        Icons.vpn_key_sharp,
                        color: Colors.blueGrey,
                        size: 25,
                      ),
                      hintText: "Enter your password",
                      hintStyle: HintStyle,
                    ),
                  ),
                ),
              ],
            ),
          ),
          //Confirm Password
          Padding(
            padding: EdgeInsets.only(top: 20, left: 30, right: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Confirm Password",
                  style: singInStyle,
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(left: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.indigo.shade300),
                  child: TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      icon: Icon(
                        Icons.vpn_key_sharp,
                        color: Colors.blueGrey,
                        size: 25,
                      ),
                      hintText: "Confirm password",
                      hintStyle: HintStyle,
                    ),
                  ),
                ),
              ],
            ),
          ),
          //Register
          Container(
              height: 55,
              padding: EdgeInsets.only(left: 5),
              margin: EdgeInsets.only(top: 80, left: 50, right: 50),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: Colors.white70,
              ),
              child: GestureDetector(
                onTap: () {},
                child: Text(
                  "Register",
                  style: GoogleFonts.lato(
                    color: Colors.indigo,
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )),
          Spacer(),
          Padding(
            padding: EdgeInsets.only(bottom: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Have an Account?",
                  style: GoogleFonts.lato(
                      color: Colors.white, fontSize: 18, letterSpacing: 0.70),
                ),
                SizedBox(
                  width: 7,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pop(
                      context,
                    );
                  },
                  child: Text("Sign In",
                      style: GoogleFonts.lato(
                        color: Colors.white,
                        fontSize: 20,
                        letterSpacing: 0.70,
                        fontWeight: FontWeight.bold,
                      )),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}