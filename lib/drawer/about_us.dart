import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AboutUs extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange.shade200,
      appBar: AppBar(
        backgroundColor: Colors.orange.shade200,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
          iconSize: 25,
          color: Colors.black,
          splashColor: Colors.indigo,
          splashRadius: 15,
        ),
      ),
      body: Padding(
        padding:   EdgeInsets.all(10),
        child: Text(
            "The about us page helps online retailers develop a relationship with customers and build trust by putting a face and a story to the name on the storefront. About us pages serve much the same purpose for retailers who sell less-common or unique items as well.",style:
          GoogleFonts.mPlus1p(color: Colors.black,fontSize: 18,fontWeight: FontWeight.w500,letterSpacing: 0.50,),),
      ),
    );
  }
}
