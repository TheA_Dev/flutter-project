import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ginseng_app/drawer/about_us.dart';
import 'package:ginseng_app/drawer/my_order_page.dart';
 import 'package:ginseng_app/pages/home_page.dart';
import 'package:google_fonts/google_fonts.dart';

import 'Profile/sign_in.dart';

class DrawerPage extends StatefulWidget {
  const DrawerPage({Key key}) : super(key: key);

  @override
  _DrawerPageState createState() => _DrawerPageState();
}

final txstyle = GoogleFonts.mPlus1p(
  letterSpacing:1,
    color: Colors.white, fontWeight: FontWeight.w600, fontSize: 20);
final styleB = GoogleFonts.lato(
    color: Colors.black45, fontSize: 15, fontWeight: FontWeight.w600);

class _DrawerPageState extends State<DrawerPage> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child:  Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage("assets/more/dark_bg.jpg"))),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: ListView(
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            padding: EdgeInsets.only(),
            children: [
              //Header
              build_drawerHeader(),
              //Home
              ListTile(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (_)=>HomePage()));
                },
                leading: Image(
                  color: Colors.white,
                  height: 30,
                  width: 30,
                  image: AssetImage("assets/icons/home.png"),
                ),
                title: Text(
                  "HOME",
                  style: txstyle,
                ),
              ),
              //My Profile
              ListTile(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (_)=>Sign_In()));
                },
                leading: Image(
                  color: Colors.white,
                  height: 30,
                  width: 30,
                  image: AssetImage("assets/icons/acc.png"),
                ),
                title: Text(
                  "MY PROFILE",
                  style: txstyle,
                ),
              ),
              //My Order
              ListTile(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (_)=>MyOrderPage()));
                },
                 leading: Image(
                  color: Colors.white,
                  height: 30,
                  width: 30,
                  image: AssetImage("assets/icons/order.png"),
                ),
                title: Text(
                  "MY ORDER",
                  style: txstyle,
                ),
              ),
              //My Wishlist
              ListTile(
                onTap: () {},
                leading: Image(
                  color: Colors.white,
                  height: 30,
                  width: 30,
                  image: AssetImage("assets/icons/heart.png"),
                ),
                title: Text(
                  "MY WISHLIST",
                  style: txstyle,
                ),
              ),
              //Offers
              ListTile(
                onTap: () {},
                leading: Image(
                  color: Colors.white,
                  height: 30,
                  width: 30,
                  image: AssetImage("assets/icons/offer.png"),
                ),
                title: Text(
                  "OFFER",
                  style: txstyle,
                ),
              ),
              //About us
              ListTile(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (_)=>AboutUs()));
                },
                leading: Image(
                  height: 30,
                  width: 30,
                  color: Colors.white70,
                  image: AssetImage("assets/icons/us.png"),
                ),
                title: Text(
                  "ABOUT US",
                  style: txstyle,
                ),
              ),
              //Help center
              ListTile(
                onTap: () {},
                leading: Image(
                  color: Colors.white,
                  height: 30,
                  width: 30,
                  image: AssetImage("assets/icons/qts.png"),
                ),
                title: Text(
                  "HELP CENTER",
                  style: txstyle,
                ),
              ),
              //Setting
              ListTile(
                onTap: () {},
                leading: Image(
                  color: Colors.white,
                  height: 30,
                  width: 30,
                  image: AssetImage("assets/icons/setting.png"),
                ),
                title: Text(
                  "SETTING",
                  style: txstyle,
                ),
              ),
              //Language
              ListTile(
                onTap: () {},
                leading: Image(
                  height: 30,
                  width: 30,
                  image: AssetImage("assets/icons/language.png"),
                ),
                title: Text(
                  "LANGUAGE",
                  style: txstyle,
                ),
              ),
              //Feedback , rate us , share
              Container(
                height: 70,
                margin: EdgeInsets.only(
                  top: 40,
                ),
                padding: EdgeInsets.only(left: 20, right: 20),
                width: MediaQuery.of(context).size.width,
                color: Colors.grey.shade200,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //Feed Back
                    GestureDetector(
                      onTap: () {},
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image(
                            color: Colors.grey,
                            height: 25,
                            width: 25,
                            image: AssetImage("assets/icons/fb.png"),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "Feedback",
                            style: styleB,
                          )
                        ],
                      ),
                    ),
                    //Rate us
                    InkWell(
                      onTap: () {},
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image(
                            color: Colors.grey,
                            height: 25,
                            width: 25,
                            image: AssetImage("assets/icons/star.png"),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text("Rate us", style: styleB)
                        ],
                      ),
                    ),
                    //Share
                    GestureDetector(
                      onTap: () {},
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image(
                            color: Colors.grey,
                            height: 25,
                            width: 25,
                            image: AssetImage("assets/icons/share.png"),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text("Share", style: styleB)
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              //Log out
              ListTile(
                onTap: () {},
                leading: Icon(
                  Icons.power_settings_new,
                  color: Colors.white,
                  size: 30,
                ),
                title: Text(
                  "LOGOUT",
                  style: txstyle,
                ),
                subtitle: Text(
                  "@Copyright by KyoungDong Mall",
                  style: GoogleFonts.lato(color: Colors.grey.shade400),
                ),
              ),
            ],
          ),
        ),);
  }

  DrawerHeader build_drawerHeader() {
    return DrawerHeader(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage("assets/more/bg.jpg"))),
                child: Stack(
                  children: [
                    //Hello customer
                    Positioned(
                      top: 40,
                      left: 0,
                      child: RichText(
                        text: TextSpan(
                            text: "Hello,\n",
                            style: GoogleFonts.marvel(
                                color: Colors.white,
                                fontSize: 30,
                                letterSpacing: 2.0,
                                fontWeight: FontWeight.w500),
                            children: [
                              TextSpan(
                                text: "General Customer",
                                style: GoogleFonts.marvel(
                                  color: Colors.white,
                                  letterSpacing: 0,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 35,
                                ),
                              ),
                            ]),
                      ),
                    ),
                    //Profile
                    Positioned(
                        top: 0,
                        right: 0,
                        child: CircleAvatar(
                          radius: 35,
                          backgroundColor: Colors.indigo,
                          backgroundImage: AssetImage("assets/more/min.png"),
                        )),
                  ],
                ),
              );
  }

}
