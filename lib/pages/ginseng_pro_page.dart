import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ginseng_app/class_items.dart';
import 'package:ginseng_app/details/ginseng_details.dart';
import 'package:google_fonts/google_fonts.dart';
import 'home_page.dart';

class GinsengPage extends StatefulWidget {
  final price;
  final image;
  final name;
  final size;

  const GinsengPage({Key key, this.price, this.image, this.name, this.size})
      : super(key: key);

  @override
  _GinsengPageState createState() => _GinsengPageState();
}

class _GinsengPageState extends State<GinsengPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            automaticallyImplyLeading: false,
            pinned: true,
            backgroundColor: Colors.white,
            toolbarHeight: 70,
            centerTitle: true,
            leading: IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(Icons.arrow_back_ios),
              iconSize: 30,
              color: Colors.black,
              splashRadius: 20,
              splashColor: Colors.indigo,
              padding: EdgeInsets.only(left: 20),
            ),
            title: Text(
              'GINSEGN',
              style: GoogleFonts.lato(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 30,
                letterSpacing: 2.50,
              ),
            ),
            actions: [
              Padding(
                padding: EdgeInsets.only(right: 10),
                child: InkWell(
                  onTap: () {},
                  splashColor: Colors.indigo,
                  child: Image(
                    height: 35,
                    width: 35,
                    color: Colors.black,
                    image: AssetImage("assets/icons/heart.png"),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(right: 20),
                child: InkWell(
                  onTap: () {},
                  splashColor: Colors.indigo,
                  borderRadius: BorderRadius.circular(50),
                  child: Image(
                    height: 30,
                    width: 30,
                    color: Colors.black,
                    image: AssetImage("assets/icons/bag.png"),
                  ),
                ),
              ),
            ],
          ),
          Grid_index_from_newLaunch(),
        ],
      ),
    );
  }

  SliverPadding Grid_index_from_newLaunch() {
    return SliverPadding(
      padding: EdgeInsets.only(left: 40, top: 10),
      sliver: SliverGrid(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5,
          mainAxisExtent: 250,
        ),
        delegate: SliverChildBuilderDelegate(
          (context, index) {
            return InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (_) => GinsengDetails()));
              },
              child: ProductItemWidget(
                index: index,
                products: Ginseng,
              ),
            );
          },
          childCount: Ginseng.length,
        ),
      ),
    );
  }
}
