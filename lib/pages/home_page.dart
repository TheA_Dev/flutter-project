import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter/material.dart';
import 'package:ginseng_app/class_items.dart';
import 'package:ginseng_app/details/ginseng_details.dart';
import 'package:ginseng_app/drawer/drawer_page.dart';
import 'package:google_fonts/google_fonts.dart';

import 'ginseng_pro_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  double imgBrgHeight = 200;
  final txstyle = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 30,
    fontWeight: FontWeight.bold,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      drawerScrimColor: Colors.white70,
      drawer: DrawerPage(),
      body: CustomScrollView(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        slivers: [
          //AppBar
          __Build_AppBar(context),
          //Discount categories Slider
          one_Slider(),
          //Text Newly Launched
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.only(left: 20, top: 10, bottom: 10),
              child: Text(
                'Newly Launched',
                style: txstyle,
              ),
            ),
          ),
          //Products Sales Newly Launched
          _newLauach(context),
          //Text Featured Products
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.only(left: 20, top: 10, bottom: 10),
              child: Text(
                'Featured Products',
                style: txstyle,
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: 250,
              width: MediaQuery.of(context).size.width,
              //color: Colors.indigo,
              child: ListView.builder(
                  physics: BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  itemCount: More_By.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                        splashColor: Colors.indigo.shade200,
                        borderRadius: BorderRadius.circular(5),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => GinsengDetails()));
                        },
                        child: Container(
                          width: 150,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              //Image
                              Container(
                                  height: 150,
                                  width: 150,
                                  margin: EdgeInsets.all(3),
                                  decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(8),
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image:
                                            AssetImage(More_By[index]['img'])),
                                  )),
                              //Name
                              Text(
                                products[index]['name'],
                                style: GoogleFonts.lato(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              const SizedBox(
                                height: 2.5,
                              ),
                              //Size
                              Text(
                                products[index]['size'],
                                style: GoogleFonts.lato(
                                  color: Colors.blueGrey,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              //Price & Rating
                              Expanded(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    //Price
                                    Text(
                                      Ginseng[index]['price'],
                                      style: GoogleFonts.lato(
                                        color: Colors.black,
                                        fontSize: 15.6,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    //Rating
                                    Container(
                                      padding: const EdgeInsets.only(
                                          top: 2, bottom: 2, left: 5, right: 5),
                                      decoration: BoxDecoration(
                                          color: Colors.green.shade500,
                                          borderRadius:
                                              BorderRadius.circular(3)),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text(
                                            "4.2",
                                            style: GoogleFonts.lato(
                                              color: Colors.white,
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          const SizedBox(
                                            width: 5,
                                          ),
                                          const Icon(
                                            Icons.star,
                                            size: 13,
                                            color: Colors.white,
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ));
                  }),
            ),
          )
        ],
      ),
    );
  }

  SliverToBoxAdapter __Build_AppBar(BuildContext context) {
    return SliverToBoxAdapter(
      child: Stack(
        children: [
          //Search and drawer and background
          Container(
              height: 200,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage("assets/more/bg.jpg"))),
              child: Stack(
                children: [
                  Container(
                    height: 50,
                    margin: EdgeInsets.only(top: 70, left: 20, right: 20),
                    padding: EdgeInsets.only(left: 10, right: 10),
                    //width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5)),
                    child: Row(
                      children: [
                        IconButton(
                          onPressed: () {
                            _globalKey.currentState.openDrawer();
                          },
                          icon: Image(
                            height: 30,
                            width: 30,
                            color: Colors.black,
                            image: AssetImage("assets/icons/menu.png"),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: TextField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Search here",
                              hintStyle: GoogleFonts.averageSans(
                                  color: Colors.blueGrey,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 0.60,
                                  fontSize: 15),
                            ),
                          ),
                        ),
                        //Notification
                        Container(
                          height: 50,
                          width: 45,
                          child: Stack(
                            children: [
                              //bag
                              InkWell(
                                  onTap: () {},
                                  child: Center(
                                    child: Image.asset(
                                      "assets/icons/bag.png",
                                      height: 30,
                                      width: 30,
                                      color: Colors.black,
                                    ),
                                  )),
                              //Pop num
                              Positioned(
                                right: 0,
                                top: 7,
                                child: GestureDetector(
                                  onTap: () {},
                                  child: Container(
                                    //margin: EdgeInsets.only(bottom: 18),
                                    height: 20,
                                    width: 20,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: Colors.red.shade700,
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                    child: Text(
                                      "3",
                                      style: GoogleFonts.lato(
                                          color: Colors.white,
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
          //Scroll Categories Horizontal
          Container(
              margin: EdgeInsets.only(top: 150),
              height: 100,
              width: MediaQuery.of(context).size.width,
              child: ListView.builder(
                  itemCount: categories.length,
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (_) => GinsengPage()));
                      },
                      splashColor: Colors.indigo,
                      borderRadius: BorderRadius.circular(20),
                      child: Container(
                        margin: EdgeInsets.only(
                          left: 20,
                        ),
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                colors: [
                                  Colors.blueGrey,
                                  Colors.grey,
                                ]),
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(categories[index]['img']))),
                        child: Padding(
                          padding: EdgeInsets.only(top: 5, left: 10),
                          child: Text(
                            categories[index]['name'],
                            style: GoogleFonts.averageSans(
                                color: Colors.white,
                                fontSize: 11,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 1),
                          ),
                        ),
                      ),
                    );
                  })),
        ],
      ),
    );
  }

  SliverToBoxAdapter one_Slider() {
    return SliverToBoxAdapter(
      child: CarouselSlider(
        options: CarouselOptions(
            height: 250,
            autoPlayAnimationDuration: Duration(seconds: 1),
            viewportFraction: 1,
            initialPage: 0,
            autoPlay: true),
        items: [
          Padding(
            padding: EdgeInsets.only(top: 10),
            child: Stack(
              children: [
                Image(
                    height: 250,
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.cover,
                    image: AssetImage("assets/slider/lee.png")),
                //Details
                Positioned(
                    right: 20,
                    top: 55,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "30% OFF",
                          style: GoogleFonts.averageSans(
                            color: Colors.white,
                            fontSize: 25,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Mid Year Sale",
                          style: GoogleFonts.averageSans(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          "Ginseng Everytime",
                          style: GoogleFonts.averageSans(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ))
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _build_swiper_disc() {
    return SliverToBoxAdapter(
      child: Expanded(
          child: Container(
        height: 230,
        margin: EdgeInsets.only(top: 15),
        color: Colors.indigo,
        child: Swiper(
          itemCount: swipBarDisc.length,
          autoplay: true,
          pagination: SwiperPagination(alignment: Alignment.bottomRight),
          itemBuilder: (context, index) {
            return Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(swipBarDisc[index]['img']))),
              child: Padding(
                padding: EdgeInsets.only(left: 210, top: 60),
                child: Column(
                  children: [
                    Text(
                      swipBarDisc[index]['disc'],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 27,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      swipBarDisc[index]['opt'],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      swipBarDisc[index]['pro'],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 21,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      )),
    );
  }

  Widget _newLauach(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        height: 250,
        //color: Colors.indigo,
        width: MediaQuery.of(context).size.width,
        child: ListView.builder(
            itemCount: Newly_Launched.length,
            physics: BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return Container(
                margin: const EdgeInsets.only(left: 10),
                child: ProductItemWidget(
                  index: index,
                  products: Newly_Launched,
                ),
              );
            }),
      ),
    );
  }
}

class ProductItemWidget extends StatelessWidget {
  final int index;
  final List<Map<String, dynamic>> products;

  const ProductItemWidget({
    Key key,
    this.index,
    this.products,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.indigo.shade200,
      borderRadius: BorderRadius.circular(5),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => GinsengDetails(
                      size: products[index]['size'],
                      image: products[index]['img'],
                      name: products[index]['name'],
                      price: products[index]['price'],
                    )));
      },
      child: Container(
        //Use this container to maintain alignment
        //color: Colors.indigo,
        //height: 150,
        width: 150,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //Image of Products
            Container(
              height: 150,
              width: 150,
              //set height & width to fit every single image
              margin: EdgeInsets.only(bottom: 10, top: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  gradient:
                      LinearGradient(begin: Alignment.bottomRight, colors: [
                    Colors.grey.shade300,
                    Colors.grey.shade200,
                  ])),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: Image.asset(
                  products[index]['img'],
                  fit: BoxFit.cover,
                  alignment: Alignment.center,
                ),
              ),
            ),
            //Name
            Text(
              products[index]['name'],
              style: GoogleFonts.lato(
                color: Colors.black,
                fontSize: 15,
                fontWeight: FontWeight.w700,
              ),
            ),
            const SizedBox(
              height: 2.5,
            ),
            //Size
            Text(
              products[index]['size'],
              style: GoogleFonts.lato(
                color: Colors.blueGrey,
                fontSize: 13,
                fontWeight: FontWeight.w600,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            //Price & Rating
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //Price
                  Text(
                    Ginseng[index]['price'],
                    style: GoogleFonts.lato(
                      color: Colors.black,
                      fontSize: 15.6,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  //Rating
                  Container(
                    padding: const EdgeInsets.only(
                        top: 2, bottom: 2, left: 5, right: 5),
                    decoration: BoxDecoration(
                        color: Colors.green.shade500,
                        borderRadius: BorderRadius.circular(3)),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          "4.2",
                          style: GoogleFonts.lato(
                            color: Colors.white,
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        const Icon(
                          Icons.star,
                          size: 13,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
