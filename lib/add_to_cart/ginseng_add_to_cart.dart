import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ginseng_app/class_items.dart';
import 'package:google_fonts/google_fonts.dart';

class Add_to_Cart extends StatefulWidget {
  @override
  _Add_to_CartState createState() => _Add_to_CartState();
}

class _Add_to_CartState extends State<Add_to_Cart> {
  int counter = 0;

  void _increment() {
    setState(() {
      counter++;
    });
  }

  void _decrement() {
    setState(() {
      counter--;
    });
  }

  final txstyle = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 18,
    fontWeight: FontWeight.bold,
  );
  final stylePromote = GoogleFonts.lato(
    color: Colors.black,
    fontSize: 18,
    fontWeight: FontWeight.w600,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Build_AppBar(context),
          Flexible(
            child: CustomScrollView(
              physics: BouncingScrollPhysics(),
              slivers: [
                //Sliver List Build
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (context, i) {
                      return products_build(context, i);
                    },
                    childCount: addToCart.length,
                  ),
                ),
                //Sliver list add promote code
              ],
            ),
          ),
          __build_bottom_promotion(context),
        ],
      ),
    );
  }

  Column __build_bottom_promotion(BuildContext context) {
    return Column(
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //Promote Code & APPLY
            Container(
                padding: EdgeInsets.only(left: 20, right: 20),
                width: MediaQuery.of(context).size.width,
                height: 63,
                color: Colors.grey.shade200,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Add Promocode",
                      style: GoogleFonts.lato(
                        color: Colors.blueGrey,
                        fontSize: 19,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    InkWell(
                      onTap: () {},
                      child: Text(
                        "APPLY",
                        style: GoogleFonts.lato(
                            color: Colors.blue.shade800,
                            fontSize: 19,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 0.50),
                      ),
                    ),
                  ],
                )),
            //Cart Total
            Padding(
              padding: EdgeInsets.only(left: 20,right: 20,top: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Cart Total", style: stylePromote),
                  Text(
                    "\$ 145.00",
                    style: stylePromote,
                  )
                ],
              ),
            ),
            //Delivery fee
            Padding(
              padding: EdgeInsets.only(left: 20,right: 20,top: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Delivery Fee",
                    style: stylePromote,
                  ),
                  Text(
                    "\$ 1.50",
                    style: stylePromote,
                  )
                ],
              ),
            ),
            //Promotcode
            Padding(
              padding: EdgeInsets.only(left: 20,right: 20,top: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Promocode",
                    style: stylePromote,
                  ),
                  Text(
                    "- \$ 5.00",
                    style: stylePromote,
                  )
                ],
              ),
            ),
            //Checkout now
            Container(
              height: 68,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(left: 20,),
              margin: EdgeInsets.only(top: 15),
              color: Colors.cyan.shade600,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Checkout Now",
                    style: GoogleFonts.lato(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  //total
                  Row(
                    children: [
                      Text(
                        "Total",
                        style: GoogleFonts.lato(
                          color: Colors.blueGrey,
                          fontSize: 17,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(width: 10,),
                      Text(
                        "\$145.00",
                        style: GoogleFonts.lato(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      IconButton(
                          icon: Icon(Icons.arrow_forward_ios),
                          color: Colors.white,
                          iconSize: 25,
                          onPressed: () {})
                    ],
                  ),
                ],
              ),
            ),
          ],
        );
  }

  Container products_build(BuildContext context, int i) {
    return Container(
      width: MediaQuery.of(context).size.width,
      //color: Colors.indigo,
      padding: EdgeInsets.only(left: 15, top: 15, bottom: 10),
      child: Row(
        children: [
          //image
          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
                color: Colors.grey.shade300,
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                    fit: BoxFit.cover,
                    alignment: Alignment.center,
                    image: AssetImage(addToCart[i]['img']))),
          ),
          //Build Details [name,size,counter,price]
          Expanded(
              child: Container(
            padding: EdgeInsets.only(left: 15),
            height: 100,
            //color: Colors.indigo,
            child: Stack(
              children: [
                //name
                Positioned(
                    top: 0,
                    child: Text(
                      addToCart[i]['name'],
                      style: txstyle,
                    )),
                //size
                Positioned(
                    top: 30,
                    child: Text(
                      addToCart[i]['size'],
                      style: GoogleFonts.lato(
                          fontSize: 14,
                          color: Colors.blueGrey,
                          fontWeight: FontWeight.w800),
                    )),
                //Counter
                Positioned(
                  bottom: 0,
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: () {
                          _decrement();
                        },
                        icon: Icon(Icons.remove_circle_outline),
                        color: Colors.blueGrey.shade500,
                        iconSize: 30,
                        splashColor: Colors.blueGrey,
                        splashRadius: 25,
                      ),
                      //SizedBox(width: 5,),
                      Text(
                        "$counter",
                        style: GoogleFonts.lato(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      // SizedBox(width: 5,),
                      IconButton(
                        onPressed: () {
                          _increment();
                        },
                        icon: Icon(Icons.add_circle_outline),
                        color: Colors.blueGrey.shade500,
                        iconSize: 30,
                        splashColor: Colors.blueGrey,
                        splashRadius: 25,
                      ),
                    ],
                  ),
                ),
                //Price
                Positioned(
                    bottom: 10,
                    right: 10,
                    child: Text(
                      addToCart[i]['price'],
                      style: txstyle,
                    )),
              ],
            ),
          ))
        ],
      ),
    );
  }

  Container Build_AppBar(BuildContext context) {
    return Container(
      height: 110,
      //color: Colors.indigo,
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Row(
          children: [
            IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              color: Colors.black,
              iconSize: 30,
              tooltip: "Back",
              splashColor: Colors.indigo,
              icon: Icon(Icons.arrow_back_ios),
            ),
            SizedBox(
              width: 80,
            ),
            Text(
              'YOUR CART',
              style: GoogleFonts.mPlus1p(
                  color: Colors.black,
                  fontSize: 24,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }

 }
