import 'package:flutter/material.dart';

class ClassItems extends StatelessWidget {
  const ClassItems({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
    );
  }
}

final More_By = [
  {
    "img": "assets/moreBy/fd.jpg",
  },
  {
    "img": "assets/moreBy/w.jpg",
  },
  {
    "img": "assets/moreBy/w1.jpg",
  },
  {
    "img": "assets/moreBy/drink.jpg",
  },
  {
    "img": "assets/moreBy/fd1.jpg",
  },
];
final swipBarDisc = [
  {
    "img": "assets/slider/lee.png",
    "disc": "30% OFF",
    "opt": "Mid Year Sale",
    "pro": "Ginseng Everytime",
  },
  {
    "img": "assets/slider/r2.png",
    "disc": "20% OFF",
    "opt": "Over Year Sale",
    "pro": "Shop Now",
  },
  {
    "img": "assets/slider/r1.png",
    "disc": "50% OFF",
    "opt": "Over 10 Days",
    "pro": "Beauty\'s Here",
  },
  {
    "img": "assets/slider/fd.jpg",
    "disc": "45% OFF",
    "opt": "Over Mid Day",
    "pro": "Food Everywhere",
  },
];
final addToCart = [
  {
    "img": "assets/Ginseng/ginseng1.png",
    "name": "Ginseng Extract",
    "size": "Size 240g",
    "price": "\$142.00",
  },
  {
    "img": "assets/moreBy/w1.jpg",
    "name": "Pure Water",
    "size": "Size 2.00lm",
    "price": "\$0.90",
  },
  {
    "img": "assets/obsidian/ob0.png",
    "name": "Shampoo of the Year",
    "size": "Size 100lm",
    "price": "\$33.50",
  },{
    "img": "assets/Ginseng/ginseng1.png",
    "name": "Ginseng Extract",
    "size": "Size 240g",
    "price": "\$142.00",
  },
  {
    "img": "assets/moreBy/w1.jpg",
    "name": "Pure Water",
    "size": "Size 2.00lm",
    "price": "\$0.90",
  },
  {
    "img": "assets/obsidian/ob0.png",
    "name": "Shampoo of the Year",
    "size": "Size 100lm",
    "price": "\$33.50",
  },
];
final Newly_Launched = [
  {
    "img": "assets/obsidian/ob0.png",
    "name": "Obsidian Shampoo",
    "size": "Size 1000ml",
    "price": "\$23.00",
  },
  {
    "img": "assets/Ginseng/ginseng2.png",
    "name": "Ginseng Extract",
    "size": "Size 160g",
    "price": "\$240.00",
  },
  {
    "img": "assets/Categ/food.jpg",
    "name": "FOOD YOU LOVE",
    "size": "Size 160g",
    "price": "\$18.00",
  },
];

// general Use
final products = [
  {
    "img": "assets/Ginseng/ginseng0.png",
    "name": "Ginseng Extract Mid",
    "size": "Size 100g",
    "price": "\$123.00",
  },
  {
    "img": "assets/Ginseng/ginseng1.png",
    "name": "Ginseng Extract",
    "size": "Size 240g",
    "price": "\$242.00",
  },
  {
    "img": "assets/Ginseng/ginseng2.png",
    "name": "Ginseng Extract Pills",
    "size": "Size 160g",
    "price": "\$240.00",
  },
  {
    "img": "assets/Ginseng/ginseng3.png",
    "name": "Ginseng Extract Tea",
    "size": "Size 10p",
    "price": "\$123.00",
  },
  {
    "img": "assets/Ginseng/ginseng4.png",
    "name": "Ginseng Honeyed",
    "size": "Size 20g",
    "price": "\$98.00",
  },
];

final Ginseng = [
  {
    "img": "assets/Ginseng/ginseng0.png",
    "name": "Ginseng Extract Mid",
    "size": "Size 100g",
    "price": "\$123.00",
  },
  {
    "img": "assets/Ginseng/ginseng2.png",
    "name": "Ginseng Extract Pills",
    "size": "Size 160g",
    "price": "\$240.00",
  },
  {
    "img": "assets/Ginseng/ginseng1.png",
    "name": "Ginseng Extract",
    "size": "Size 240g",
    "price": "\$242.00",
  },
  {
    "img": "assets/Ginseng/ginseng4.png",
    "name": "Ginseng Honeyed Slices",
    "size": "Size 20g",
    "price": "\$98.00",
  },
  {
    "img": "assets/Ginseng/ginseng2.png",
    "name": "Ginseng Extract Pills",
    "size": "Size 160g",
    "price": "\$240.00",
  },
  {
    "img": "assets/Ginseng/ginseng3.png",
    "name": "Ginseng Extract Tea",
    "size": "Size 10p",
    "price": "\$123.00",
  },
  {
    "img": "assets/Ginseng/ginseng4.png",
    "name": "Ginseng Honeyedww",
    "size": "Size 20g",
    "price": "\$98.00",
  },
  {
    "img": "assets/Ginseng/ginseng0.png",
    "name": "Ginseng Extract Mid",
    "size": "Size 100g",
    "price": "\$123.00",
  },
];
final categories = [
  {
    "img": "assets/Categ/hoo.png",
    "name": "SUPPLEMENT",
  },
  {
    "img": "assets/Categ/fashion.png",
    "name": "FASHION",
  },
  {
    "img": "assets/Categ/beauty.png",
    "name": "BEAUTY",
  },
  {
    "img": "assets/Categ/food.jpg",
    "name": "FOOD",
  },
];
