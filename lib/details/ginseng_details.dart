import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:ginseng_app/add_to_cart/ginseng_add_to_cart.dart';
import 'package:ginseng_app/class_items.dart';
import 'package:google_fonts/google_fonts.dart';

class GinsengDetails extends StatefulWidget {
  final price;
  final image;
  final name;
  final size;

  const GinsengDetails({Key key, this.price, this.image, this.name, this.size})
      : super(key: key);

  @override
  _GinsengDetailsState createState() => _GinsengDetailsState();
}

final textdetials =
    "Support null safety (Null safety isn't a breaking change and is Backward compatible meaning"
    "you can use it with non-null safe code too)"
    "Update example code to null safety and add Dark theme support and controller support to"
    "indicators in on of the examples and also fix overflow errors.";
final txstyle = GoogleFonts.lato(
  color: Colors.black,
  fontSize: 23,
  fontWeight: FontWeight.bold,
);
final blueGreystyle = GoogleFonts.lato(
  color: Colors.blueGrey,
  fontSize: 15,
  fontWeight: FontWeight.w400,
);

class _GinsengDetailsState extends State<GinsengDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        slivers: [
          //Images & icons/Swiper
          SliverToBoxAdapter(child: _build_Swiper(context)),
          //Details name price add to cart...
          SliverToBoxAdapter(child: _build_details(context)),
          //more by seller
          SliverToBoxAdapter(
            child: Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RichText(
                    text: TextSpan(
                        text: "More by",
                        style: GoogleFonts.lato(
                          color: Colors.grey.shade800,
                          fontSize: 23,
                          fontWeight: FontWeight.w500,
                        ),
                        children: [
                          WidgetSpan(
                              child: SizedBox(
                            width: 10,
                          )),
                          TextSpan(
                            text: "Seller",
                            style: GoogleFonts.lato(
                              color: Colors.black,
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ]),
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Text(
                      "VIEW ALL",
                      style: GoogleFonts.mPlus1p(
                        color: Colors.blueGrey,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          //More Sale Products
          SliverGrid(
            delegate: SliverChildBuilderDelegate((context, index) {
              return Container(
                margin:
                    EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.grey.shade300,
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(More_By[index]['img']))),
              );
            }, childCount: More_By.length),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
            ),
          ),
        ],
      ),
    );
  }

  Stack _build_Swiper(BuildContext context) {
    return Stack(
      children: [
        //Swiper
        Container(
          height: 300,
          width: MediaQuery.of(context).size.width,
         // color: Colors.blueGrey,
          child: Swiper(
            itemCount: Slider.length,
            autoplay: true,
            loop: true,
            pagination: SwiperPagination(
              alignment: Alignment.bottomRight,
            ),
            itemBuilder: (context, index) {
              return FittedBox(
                fit: BoxFit.cover,
                child: Image(
                    alignment: Alignment.bottomCenter,
                    image: AssetImage(Slider[index]['img'])),
              );
            },
          ),
        ),
        //Icon Back
        Positioned(
          left: 15,
          top: 45,
          child: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.arrow_back_ios),
            iconSize: 40,
            color: Colors.white,
            tooltip: 'Back',
            splashColor: Colors.indigo.shade300,
          ),
        ),
        //Love
        Positioned(
          right: 15,
          top: 45,
          child: IconButton(
            onPressed: () {},
            icon: Icon(Icons.favorite),
            iconSize: 40,
            color: Colors.white,
            tooltip: 'Lover',
          ),
        ),
      ],
    );
  }

  Column _build_details(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        //name , price , rating , reviews , text details
        Padding(
          padding: EdgeInsets.only(left: 20, top: 15, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //name
              Text(
                "${widget.name}",
                style: txstyle,
              ),
              SizedBox(
                height: 8,
              ),
              //Rating &
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "CheongKwanJang",
                    style: blueGreystyle,
                  ),
                  //rating
                  Container(
                    padding: const EdgeInsets.only(
                        top: 2, bottom: 2, left: 5, right: 5),
                    decoration: BoxDecoration(
                        color: Colors.green.shade500,
                        borderRadius: BorderRadius.circular(3)),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          "4.2",
                          style: GoogleFonts.lato(
                            color: Colors.white,
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        const Icon(
                          Icons.star,
                          size: 13,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 18,
              ),
              //Price & Reviews
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "${widget.price}",
                    style: txstyle,
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Read all 124 reviews",
                          style: blueGreystyle,
                        ),
                        SizedBox(
                          width: 3,
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.blueGrey,
                          size: 14,
                        )
                      ],
                    ),
                  ),
                ],
              ),
              //text details
              Container(
                margin: EdgeInsets.only(top: 15, bottom: 15),
                child: Text(
                  textdetials,
                  style: GoogleFonts.lato(
                    color: Colors.grey.shade900,
                    fontSize: 17,
                    height: 1.50,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        ),
        //add to cart
        InkWell(
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (_) => Add_to_Cart()));
          },
          radius: 50,
          child: Container(
            height: 60,
            width: MediaQuery.of(context).size.width,
            color: Colors.cyan.shade600,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/icons/bag.png",
                  height: 30,
                  width: 30,
                  color: Colors.white,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "ADD TO CART",
                  style: GoogleFonts.lato(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}

final Slider = [
  {
    "img" : "assets/slider/G1.jpg",
  },  {
    "img" : "assets/slider/G2.jpg",
  },  {
    "img" : "assets/slider/min.jpg",
  },
];